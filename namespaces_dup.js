var namespaces_dup =
[
    [ "encoder", "namespaceencoder.html", "namespaceencoder" ],
    [ "lab1", null, [
      [ "displayBalance", "lab1_8py.html#aecc533dd714d8b526f4921d7d2340f58", null ],
      [ "getChange", "lab1_8py.html#abe1392e72a5cac153081cd625c6e4760", null ],
      [ "on_keypress", "lab1_8py.html#ad6deed31d7a1a16ee6dfd6f3b7932b5a", null ],
      [ "printWelcome", "lab1_8py.html#a2fbfb15f1df665f384cc3e3c877c396d", null ],
      [ "takeUserInput", "lab1_8py.html#a96c5bc95bb889839adb4f3adc581ebfd", null ],
      [ "balance", "lab1_8py.html#a7bccd61e28fe222e4d9f42fe8a5bc855", null ],
      [ "COIN", "lab1_8py.html#a23e20e7b1fa1ec5065815f06f1e852e4", null ],
      [ "coin_amount", "lab1_8py.html#a451e621dec3b800823e8ad210ec8d074", null ],
      [ "DRINK", "lab1_8py.html#ab2e15015d873eaeccf0858b23f8d2ac1", null ],
      [ "EJECT", "lab1_8py.html#ac5330fd705d957aa9ecd1fa68bebc7bd", null ],
      [ "EMPTY", "lab1_8py.html#abd01824645a7f63ebbe5c6d3f5f854d4", null ],
      [ "INVALID", "lab1_8py.html#a2bb0f04a2e44c0a938e9d23b70834c31", null ],
      [ "pushed_key", "lab1_8py.html#acfbb04297b62de4ad915f29e26efee19", null ],
      [ "return_denoms", "lab1_8py.html#aa18724fed8dffb2e98b814f2fb8afea9", null ],
      [ "selected_drink_amount", "lab1_8py.html#a3980b37db43aec4bbc662db8e179c0e4", null ],
      [ "state", "lab1_8py.html#a40c9aae6659688d7bf23acc6ef902f67", null ],
      [ "user_input", "lab1_8py.html#a2aa0bf8658e296c93569af558b014e2b", null ]
    ] ],
    [ "lab2", null, [
      [ "buttonPressISR", "lab2_8py.html#a3cc0bd81c2d3097ae25d3e1387ce21ac", null ],
      [ "timerCB", "lab2_8py.html#abdd09b2ad81ff3d5b59f14dcf2603958", null ],
      [ "blinkLED", "lab2_8py.html#a3103fbc3f6ea3b5707b246476ce9a695", null ],
      [ "buttonExtInt", "lab2_8py.html#a6efdaf24224257f03b2bbed54dadb897", null ],
      [ "buttonPressed", "lab2_8py.html#a1f6a761075e9ee5aeadf0eb580b33f9c", null ],
      [ "ledOn", "lab2_8py.html#a1abe68829ea82d6657313533d740de65", null ],
      [ "reactionTime", "lab2_8py.html#acc4bf37a85e770c36dfe96f0dfe3bece", null ],
      [ "reactionTimer", "lab2_8py.html#afe90ccb5d3d00b84f993b4738585f92a", null ]
    ] ],
    [ "ui_frontend", null, [
      [ "recvMessage", "ui__frontend_8py.html#a7efd5cf96cb6a5a1c7bf0d53834aca6d", null ],
      [ "sendChar", "ui__frontend_8py.html#af72e4bdd5120d999ea987172085a731d", null ],
      [ "csvFile", "ui__frontend_8py.html#a4d4191c366201b2471f963905815cfeb", null ],
      [ "inv", "ui__frontend_8py.html#aa61988db41b05e208cc60a4e5b61805d", null ],
      [ "message", "ui__frontend_8py.html#a41e6695dfcceb185517622d5cccd2c54", null ],
      [ "ser", "ui__frontend_8py.html#a844937ed541078fbd0f3df7562dcf107", null ],
      [ "times", "ui__frontend_8py.html#add07a823da065ed4289949f292f6762c", null ],
      [ "values", "ui__frontend_8py.html#af1eb65fb444695b66159e8e18e385912", null ]
    ] ]
];